import discord
import os
import random
import sqlite3
from discord.ext import commands
from dotenv import load_dotenv

admin = [0]

load_dotenv()
TOKEN = os.getenv('TOKEN')

intents = discord.Intents.default()

bot = commands.Bot(command_prefix='$', intents=intents)

@bot.event
async def on_ready():
    print('We have logged in as {0.user}'.format(bot))

#@bot.command()
#async def create(ctx):
#    connection = sqlite3.connect('game.db')
#    cursor = connection.cursor()

#    cursor.execute("create table party (userid INTEGER PRIMARY KEY, stars INTEGER DEFAULT 0 CHECK(stars >= 0), coins INTEGER DEFAULT 0 CHECK(coins >= 0) )")
    #cursor.execute("select * from bonks where user=:id", {"id": userid})
    #bonkcount = cursor.fetchone()[1]-1
    #cursor.execute("update bonks set bonks=:bc where user=:id", {"bc": bonkcount, "id": userid})
#    connection.commit()
#    connection.close()

@bot.command()
async def add(ctx, member: discord.Member):
    if ctx.author.id not in admin:
        return await ctx.send("get lost")
    connection = sqlite3.connect('game.db')
    cursor = connection.cursor()

    cursor.execute("select * from party where userid=:id", {"id": member.id})
    data=cursor.fetchone()
    if data is None:
        cursor.execute("insert into party (userid) VALUES (:id)", {"id": member.id})
        connection.commit()
        await ctx.send(f"{member.display_name} has been added to the game!")
    else:
        await ctx.send(f"{member.display_name} has already joined the party!")

@bot.command()
async def remove(ctx, member: discord.Member):
    if ctx.author.id not in admin:
        return await ctx.send("get lost")
    connection = sqlite3.connect('game.db')
    cursor = connection.cursor()

    cursor.execute("select * from party where userid=:id", {"id": member.id})
    data=cursor.fetchone()
    if data is None:
        await ctx.send(f"{member.display_name} is not playing!")
    else:
        cursor.execute("delete from party where userid=:id", {"id": member.id})
        connection.commit()
        await ctx.send(f"{member.display_name} has been removed from the game!")

@bot.command()
async def set(ctx, member: discord.Member, type, amt):
    if ctx.author.id not in admin:
        return await ctx.send("get lost")
    connection = sqlite3.connect('game.db')
    cursor = connection.cursor()

    hintmsg="Hint: Try $set @user [stars/coins] [amount]"

    if type == "stars" or type == "star":
        datatype="stars"
    elif type == "coins" or type == "coin":
        datatype="coins"
    else:
        return await ctx.send(hintmsg)
    
    if not amt.isnumeric() or int(amt) < 0:
        return await ctx.send(hintmsg)

    cursor.execute("select * from party where userid=:id", {"id": member.id})
    data=cursor.fetchone()
    if data is None:
        await ctx.send(f"{member.display_name} is not playing!")
    else:
        cursor.execute(f"update party set {datatype}={amt} where userid={member.id}")
        connection.commit()
        await ctx.send(f"{member.display_name} now has {amt} {datatype}!")

@bot.command()
async def give(ctx, member: discord.Member, type, amt):
    if ctx.author.id not in admin:
        return await ctx.send("get lost")
    connection = sqlite3.connect('game.db')
    cursor = connection.cursor()

    hintmsg="Hint: Try $give @user [stars/coins] [amount]"

    if type == "stars" or type == "star":
        datatype="stars"
    elif type == "coins" or type == "coin":
        datatype="coins"
    else:
        return await ctx.send(hintmsg)
    
    if not amt.isnumeric() or int(amt) < 0:
        return await ctx.send(hintmsg)

    cursor.execute(f"select {datatype} from party where userid={member.id}")
    data=cursor.fetchone()
    if data is None:
        await ctx.send(f"{member.display_name} is not playing!")
    else:
        newdata = data[0]+int(amt)
        cursor.execute(f"update party set {datatype}={newdata} where userid={member.id}")
        connection.commit()
        await ctx.send(f"{member.display_name} now has {newdata} {datatype}!")

@bot.command()
async def take(ctx, member: discord.Member, type, amt):
    if ctx.author.id not in admin:
        return await ctx.send("get lost")
    connection = sqlite3.connect('game.db')
    cursor = connection.cursor()

    hintmsg="Hint: Try $take @user [stars/coins] [amount]"

    if type == "stars" or type == "star":
        datatype="stars"
    elif type == "coins" or type == "coin":
        datatype="coins"
    else:
        return await ctx.send(hintmsg)
    
    if not amt.isnumeric() or int(amt) < 0:
        return await ctx.send(hintmsg)

    cursor.execute(f"select {datatype} from party where userid={member.id}")
    data=cursor.fetchone()
    if data is None:
        await ctx.send(f"{member.display_name} is not playing!")
    else:
        newdata = max(0,data[0]-int(amt))
        cursor.execute(f"update party set {datatype}={newdata} where userid={member.id}")
        connection.commit()
        await ctx.send(f"{member.display_name} now has {newdata} {datatype}!")

@bot.command()
async def stars(ctx, member: discord.Member=None):
    if member:
        userid = member.id
        mention = member.mention
    else:
        userid = ctx.author.id
        mention = ctx.author.mention
    
    connection = sqlite3.connect('game.db')
    cursor = connection.cursor()

    cursor.execute("select stars from party where userid=:id", {"id": userid})
    stars=cursor.fetchone()
    if stars is None:
        await ctx.send(f"{member.display_name} is not playing!")
    else:
        await ctx.send(f"{mention} currently has {stars[0]} stars!")
        connection.commit()

@bot.command()
async def coins(ctx, member: discord.Member=None):
    if member:
        userid = member.id
        mention = member.mention
    else:
        userid = ctx.author.id
        mention = ctx.author.mention
    
    connection = sqlite3.connect('game.db')
    cursor = connection.cursor()

    cursor.execute("select coins from party where userid=:id", {"id": userid})
    coins=cursor.fetchone()
    if coins is None:
        await ctx.send(f"{member.display_name} is not playing!")
    else:
        await ctx.send(f"{mention} currently has {coins[0]} coins!")
        connection.commit()

@bot.command()
async def leaderboard(ctx):
    connection = sqlite3.connect('game.db')
    cursor = connection.cursor()

    cursor.execute("select * from party order by stars desc, coins desc")
    data=cursor.fetchall()
    connection.close()

    if data is None:
        return await ctx.send("I have no records for you.")
    
    msglist = []
    msglist.append("User\t\|\tStars\t|\tCoins\t\n----------------------------\n")

    for row in data:
        user = await bot.fetch_user(row[0])
        msglist.append(f"{user.mention}\t|\t{row[1]}\t|\t{row[2]}\n")
   
    embed = discord.Embed(title="Mario Party XXX Leaderboard", description=''.join(msglist))
    await ctx.send(embed=embed)

@bot.command()
async def roll(ctx, dice="20"):
    errmsg = "learn how to use dice idk how to help you"
    if dice.isnumeric():
        await ctx.send(f"Rolling a d{dice}:")
        await ctx.send(f"You rolled a {random.randrange(1,int(dice)+1)}!")
    elif dice[0].upper() == 'D' and dice[1:].isnumeric():
        await ctx.send(f"Rolling a d{dice[1:]}:")
        await ctx.send(f"You rolled a {random.randrange(1,int(dice[1:])+1)}!")
    elif dice[0].isnumeric():
        i = 0
        while dice[i].isnumeric():
            i += 1
        if dice[i].upper() != 'D' or not dice[i+1:].isnumeric():
            return await ctx.send(errmsg)
        if int(dice[:i]) > 500:
            return await ctx.send("pls dont")
        result = "You rolled:\n"
        for _ in range(0, int(dice[:i])):
            result += str(random.randrange(1,int(dice[i+1:])+1)) + " "
        await ctx.send(f"Rolling {dice[:i]} d{dice[i+1:]}:")
        await ctx.send(result)

    else:
        await ctx.send(errmsg)

@bot.event
async def on_command_error(ctx, error):
    if isinstance(error, commands.MissingRequiredArgument):
        await ctx.send("Hint: Try $command @user [stars/coins] [amount]")

bot.run(TOKEN)